import { ArticleList } from "@/components/ArticleList";
import { db } from "@/db";
import { Box, Typography } from "@mui/joy";
import Container from "@mui/joy/Container";
// import Head from "next/head";
import Button from '@mui/joy/Button'

export default function ArticleListPage({ articleList = [] }) {
  return (
    <Container sx={{ py: 2 }}>
      <Box textAlign="right" mb={2}>
        <Button component="a" href="/add">
          Add article
        </Button>
      </Box>
      <Typography variant="h1">Article List Page</Typography>
      <ArticleList data={articleList} />
    </Container>
  );
}

export async function getServerSideProps() {
  const data = await db.listArticles();
  return {
    props: {
      articleList: data.map((item) => ({
        ...item,
        createdAt: item.createdAt.toISOString(),
      })),
    },
  };
}
