import { db } from "@/db";
import { Skeleton, Typography } from "@mui/joy";
import Container from "@mui/joy/Container";
import { parse } from "marked";
import Head from "next/head";

export default function ArticlePage({ article }) {
  return (
    <Container sx={{ py: 2 }}>
      <Head>
        <title>{[article.title, "My Journal"].join(" | ")}</title>
      </Head>
      <Typography level="h1" fontWeight={900} color="primary">
        {article.title}
      </Typography>
      <Typography level="body-sm">
        {new Date(article.createdAt).toDateString()}
      </Typography>
      <div dangerouslySetInnerHTML={{ __html: parse(article.body) }} />
    </Container>
  );
}

export async function getServerSideProps(ctx) {
  const data = await db.getArticle(ctx.params.id);

  return {
    props: {
      article: { ...data, createdAt: data.createdAt.toISOString() },
    },
  };
}
